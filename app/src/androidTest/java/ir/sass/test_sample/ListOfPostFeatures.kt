package ir.sass.test_sample

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import org.junit.Rule
import org.junit.Test

class ListOfPostFeatures {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun titleIsDisplayed(){
        assertDisplayed("List Of Posts")
    }

    @Test
    fun isRecyclerViewShown(){
        onView(withId(R.id.recyclerView))
            .check(ViewAssertions.matches(isDisplayed()))
    }

}