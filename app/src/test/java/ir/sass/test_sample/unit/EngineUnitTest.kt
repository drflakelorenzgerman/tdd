package ir.sass.test_sample.unit

import org.junit.Assert.assertEquals
import org.junit.Test

class EngineUnitTest {

    var engine = Engine()

    @Test
    fun increaseTemperatureWhenEngineIsTurnedOn(){
        val initTemperature = engine.temperature
        engine.turnOn()
        assertEquals(true, engine.temperature > initTemperature)
    }
}