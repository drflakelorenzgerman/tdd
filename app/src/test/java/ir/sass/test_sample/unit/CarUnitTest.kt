package ir.sass.test_sample.unit

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert
import org.junit.Test

class CarUnitTest {

    var engine : Engine = mock()
    var car = Car(engine,10)

    @Test
    fun carIsLoosingFuelWhenItTurnOn(){
        val initFuel = car.fuel
        car.turnOn()
        Assert.assertEquals(true, car.fuel < initFuel)
        Assert.assertEquals(false, car.fuel < 0)
    }

    @Test
    fun engineIsTurnOnWheneverTheCarIsTurnedOn(){
        car.turnOn()
        verify(engine,times(1)).turnOn()
    }

}